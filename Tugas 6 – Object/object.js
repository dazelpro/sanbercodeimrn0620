// SOAL 1
function arrayToObject(array) {
    let obj = {};
    if(array.length == 0) {
        console.log('""');
    }
    for(let i=0; i<=array.length-1; i++){
        obj = {
            firstName : array[i][0],
            lastName: array[i][1],
            gender: array[i][2],
            age: calcAge(array[i][3])
        }
        console.log((i+1)+'. '+array[i][0]+' '+array[i][1]+' : ',obj)
    }
}
function calcAge(data){
    let now = new Date();
    let thisYear = now.getFullYear();
    if(!data || data > thisYear) {
        return 'Invalid birth year';
    } else {
        return thisYear - data;
    }
}
// TEST CASE
let people = [ 
    ["Bruce", "Banner", "male", 1975], 
    ["Natasha", "Romanoff", "female"] 
];
arrayToObject(people)
let people2 = [ 
    ["Tony", "Stark", "male", 1980], 
    ["Pepper", "Pots", "female", 2023] 
];
arrayToObject(people2)
arrayToObject([])

// SOAL 2
function shoppingTime(memberId, money) {
    if(!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }

    if(money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    }

    let change = money;
    let listItem = [];

    for(let i=0; i<5; i++){
        if(change >= 1500000){
            listItem.push('Sepatu Stacattu');
            change -= 1500000;
        } else if(change >= 500000){
            listItem.push('Baju Zoro');
            change -= 500000;
        } else if(change >= 250000){
            listItem.push('Baju H&N');
            change -= 250000;
        } else if(change >= 175000){
            listItem.push('Sweater Uniklooh');
            change -= 175000;
        } else if(change >= 50000){
            listItem.push('CasingHandphone');
            change -= 50000;
            break;
        }
    }

    let objProduct = {
        memberId: memberId,
        money: money,
        listPurchased: listItem,
        changeMoney: change
    };
    return objProduct;
}

// TEST CASE
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

// SOAL 3 
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var angkot = [{},{}];
    var i=0;
    var asal = '';
    var tujuan = '';
    
    if(arrPenumpang.length == 0){
        return "[]";
    };

    for (i; i<arrPenumpang.length; i++) {
        var j = 0;
        for (j; j<arrPenumpang[i].length; j++) {
            switch (j) {
                case 0: {
                    angkot[i].penumpang = arrPenumpang[i][j];
                    break;
                } case 1: {
                    angkot[i].naikDari = arrPenumpang[i][j];
                    angkot[i].tujuan = arrPenumpang[i][j+1];
                    break;
                } case 2: {
                    asal = arrPenumpang[i][j-1];
                    tujuan = arrPenumpang[i][j];
                    var jarak = 0;
                    for (var k=0; k<rute.length; k++) {
                        if (rute[k] === asal) {
                            for (var l=k+1; l<rute.length; l++) {
                                jarak += 1;
                                if (rute[l] === tujuan) {
                                    var bayar = jarak * 2000;
                                    angkot[i].bayar = bayar;
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
    return angkot;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));


// LINK EDITOR : https://repl.it/@dazelpro/Object



