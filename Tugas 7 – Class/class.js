// SOAL 1 - RELEASE 0
class Animal {
    constructor(param) {
        this.nameAnimal = param;
        this.legsAnimal = 4;
        this.cold_bloodedAnimal = false;
    }
    get name() {
        return this.nameAnimal;
    }
    get legs() {
        return this.legsAnimal;
    }
    get cold_blooded() {
        return this.cold_bloodedAnimal;
    }
}

var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// SOAL 1 - RELEASE 1
class Ape extends Animal {
    constructor(param) {
        super(param);
        this.yellSound = 'Auooo';
    }
    yell() {
        return console.log(this.yellSound);
    }
}
var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

class Frog extends Animal {
    constructor(param) {
        super(param);
        this.jumpSound = 'hop hop';
    }
    jump() {
        return console.log(this.jumpSound);
    }
}

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// SOAL 2 - FUNCTION TO CLASS
class Clock {
    constructor(data) {
        this.template = data.template;
        // console.log(this.template);
    }
    render() {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
    
        console.log(output);
    }
    stop() {
        clearInterval(this.timer);
    }
    start() {
        this.timer = setInterval(() => this.render(), 1000)
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();


// Link Editor : https://repl.it/@dazelpro/Class




