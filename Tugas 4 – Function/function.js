// Soal 1
function teriak() {
    return "Hallo Sanbers!";
}
console.log(teriak());

// Soal 2
function kalikan(a,b) {
    return a*b;
}
let num1 = 10;
let num2 = 4;
let hasil = kalikan(num1,num2);
console.log(hasil);

// Soal 3
function introduce(a,b,c,d) {
    return "Nama saya "+a+", umur saya "+b+" tahun, alamat saya di "+c+", dan saya punya hobby yaitu "+d+"!"
}
let name = "Zeldianto"
let age = 24
let address = "Jln. Raden Saleh, Padang"
let hobby = "Rebahan"
let perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

  // Link Editor : https://repl.it/@dazelpro/Function


