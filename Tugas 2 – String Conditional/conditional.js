// Soal 1 - If Else 
var nama = ""
var peran = "" // Penyihir, Guard, Werewolf

if (nama === "") {
    console.log('Nama harus diisi!');
} else {
    if (peran === "") {
        console.log('Halo '+nama+', Pilih peranmu untuk memulai game!');
    } else {
        if (peran === "Penyihir") {
            console.log('Selamat datang di Dunia Werewolf, '+nama+'');
            console.log('Halo Penyihir '+nama+', kamu dapat melihat siapa yang menjadi werewolf!')
        } else if (peran === "Guard") {
            console.log('Selamat datang di Dunia Werewolf, '+nama+'');
            console.log('Halo Guard '+nama+', kamu akan membantu melindungi temanmu dari serangan werewolf.')
        } else if (peran === "Werewolf") {
            console.log('Selamat datang di Dunia Werewolf, '+nama+'');
            console.log('Halo Werewolf '+nama+', Kamu akan memakan mangsa setiap malam!')
        }
    }
}

// Soal 2 - Switch Case
var tanggal = 13; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 7; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1996; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch (bulan) 
{
    case 1:
        console.log(+ tanggal + ' Januari ' + tahun);
        break;
    case 2:
        console.log(+ tanggal + ' Februari ' + tahun);
        break;
    case 3:
        console.log(+ tanggal + ' Maret ' + tahun);
        break;
    case 4:
        console.log(+ tanggal + ' April ' + tahun);
        break;
    case 5:
        console.log(+ tanggal + ' Mei ' + tahun);
        break;
    case 6:
        console.log(+ tanggal + ' Juni ' + tahun);
        break;
    case 7:
        console.log(+ tanggal + ' Juli ' + tahun);
        break;
    case 8:
        console.log(+ tanggal + ' Agustus ' + tahun);
        break;
    case 9:
        console.log(+ tanggal + ' September ' + tahun);
        break;
    case 10:
        console.log(+ tanggal + ' Oktober ' + tahun);
        break;
    case 11:
        console.log(+ tanggal + ' November ' + tahun);
        break;
    case 12:
        console.log(+ tanggal + ' Desember ' + tahun);
        break;
    default:
        console.log("Nama Bulan tidak terdeteksi!");
        break;  
}


// https://repl.it/@dazelpro/condisional