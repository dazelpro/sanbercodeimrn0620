// Soal 1 Looping
var angkaPertama = 1;
console.log('LOOPING PERTAMA')
while(angkaPertama < 21) {
    if (angkaPertama%2==0) {
        console.log(angkaPertama+' - I love coding');
    }
    angkaPertama++;
}
var angkaKedua = 21;
console.log('LOOPING KEDUA')
while(angkaKedua > 0) {
    if (angkaKedua%2==0) {
        console.log(angkaKedua+' - I will become a mobile developer');
    }
    angkaKedua--;
}

// Soal 2 Looping For
for(var angka = 1; angka < 21; angka++) {
    if (angka%2!=0) {
        if (angka%3==0) {
            console.log(angka+' - I Love Coding ');
        } else {
            console.log(angka+' - Santai');
        }
    } else if (angka%2==0) {
        console.log(angka+' - Berkualitas');
    }
}

// Soal 3 Membuat Persegi Panjang
var persegiPanjang = 1;
while(persegiPanjang < 5) {
    console.log('########');
    persegiPanjang++;
}

// Soal 4 Membuat Tangga
var sikusiku = '';
for (var i=1; i<=7; i++){
    for (var j=1; j<=i; j++){
        sikusiku += '#';
    }
    sikusiku += '\n';
}
console.log(sikusiku);

// Soal 5 Membuat Papan Catur
let sharp = "#"
let space = " ";
let result = "";
let num = 8;
for ( let i=0 ;i<num; i++ ) {
    for (let j=0; j<num; j++) {
        if (i%2==0) {
            if (j%2==0) {
                result = result + sharp;
            } else {
                result = result + space;
            }
        } else {
            if ( j%2==0) {
                result = result + space;
            } else {
                result = result + sharp;
            }
        }
    }
    console.log(result);
    result = "" ;
}

// Link Editor : https://repl.it/@dazelpro/Looping