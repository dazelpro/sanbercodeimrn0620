// Soal 1 - RANGE
function range(startNum, finishNum) {
    let range = [];
    if(startNum == undefined || finishNum == undefined) {
        return -1;
    }
    if(startNum <= finishNum) {
        for(startNum; startNum<=finishNum; startNum++) {
            range.push(startNum);
        }
    }
    else {
        for(startNum; startNum>=finishNum; startNum--) {
            range.push(startNum);
        }
    }
    return range;

}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal 2 - RANGE WITH STEP
function rangeWithStep(startNum, finishNum, step) {
    let range = [];
    if(startNum <= finishNum) {
        for(startNum; startNum<=finishNum; startNum+=step) {
            range.push(startNum);
        }
    }
    else {
        for(startNum; startNum>=finishNum; startNum-=step) {
            range.push(startNum);
        }
    }
    return range;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal 3 - SUM RANGE WITH STEP
function rangeSum(startNum, finishNum, step) {
    let range = [];
    if(!step) {
        step = 1;
    }
    if(startNum == undefined) {
        return 0;
    }
    if(finishNum == undefined) {
        return 1;
    }
    if(startNum <= finishNum) {
        for(startNum; startNum<=finishNum; startNum+=step) {
            range.push(startNum);
        }
    }
    else {
        for(startNum; startNum>=finishNum; startNum-=step) {
            range.push(startNum);
        }
    }
    let total = 0;
    for (let i = 0; i < range.length; i++) {
        total += range[i];
    }
    return total;
}
console.log(rangeSum(1,10)) // 55
console.log(rangeSum(5, 50, 2)) // 621
console.log(rangeSum(15,10)) // 75
console.log(rangeSum(20, 10, 2)) // 90
console.log(rangeSum(1)) // 1
console.log(rangeSum()) // 0 

// Soal 4 - ARRAY MULTIDIMENSI
function dataHandling(data) {
    let sequence='';
    for(let i=0; i<=data.length-1; i++){
        console.log('Nomor ID : '+data[i][0]);
        console.log('Nama Lengkap : '+data[i][1]);
        console.log('TTL : '+data[i][2]+' '+data[i][3]);
        console.log('Hobi :'+data[i][4]);
        console.log('');
    }
    return sequence;
}
let input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling(input);

// Soal 5 - BALIK KATA
function balikKata(data){
    let kata='';
    
    for(let i=data.length-1; i>=0; i--){
        kata=kata+data[i];
    }
    return kata;
}
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 

// Soal 6 - METODE ARRAY
function dataHandling2(input){
    // 1
    input.splice(1, 1, 'Roman Alamsyah Elsharawy');
    input.splice(2, 1, 'Provinsi Bandar Lampung');
    input.splice(4, 1, 'Pria');
    input.splice(5, 0, 'SMA Internasional Metro');
    console.log(input);

    // 2
    let bulan = '05';
    switch(bulan) {
        case '01': {
            console.log('Januari'); 
            break; 
        }
        case '02': {
            console.log('Februari'); 
            break; 
        }
        case '03': {
            console.log('Maret');
            break; 
        }
        case '04': {
            console.log('April');
            break; 
        }
        case '05': {
            console.log('Mei');
            break; 
        }
        case '06': {
            console.log('Juni');
            break; 
        }
        case '07': {
            console.log('Juli');
            break; 
        }
        case '08': {
            console.log('Agustus');
            break; 
        }
        case '09': {
            console.log('September');
            break; 
        }
        case '10': {
            console.log('Oktober');
            break; 
        } 
        case '11': {
            console.log('November');
            break; 
        }
        case '12': {
            console.log('Desember');
            break }

        default: {
            console.log(''); }
    }

    // 3
    let tanggal = input[3];
    let tanggalSplit = tanggal.split('/');
    
    let indexTanggal = Number(tanggalSplit[0]);
    let indexBulan = Number(tanggalSplit[1]);
    let indexTahun = Number(tanggalSplit[2]);
    let indexGab = indexTanggal + ' ' + indexBulan +' ' +  indexTahun
    let indexGabSplit = indexGab.split(' ');
    indexGabSplit.sort(
        function(value1, value2) { 
            return value1 > value2 
        }
    );
    console.log(indexGabSplit);

    // 4
    let tanggalStrip = tanggal.split('/');
    console.log(tanggalStrip.join('-'));

    // 5
    let namaPanjang = input[1];
    let namaPendek = namaPanjang.slice(0, 15);
    console.log(namaPendek);
    
}
let input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 

// Link Editor : https://repl.it/@dazelpro/Array



