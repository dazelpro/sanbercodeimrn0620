
const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

// SOAL 1
class Score {
    constructor(param) {
        this.dataScore = param;
    }
    get avg() {
        for(let i=1; i<=this.dataScore.length-1; i++){
            let rata = (this.dataScore[i][1] + this.dataScore[i][2] + this.dataScore[i][3]) / 3
            console.log(`Rata-rata: ${rata}`)
        }
    }
}

var testing = new Score(data);
testing.avg;

// SOAL 2
function viewScores(data, subject) {
    let obj = {};
    let index = 0;
    if (subject == "quiz-1") {
        index = 1;
    } else if (subject == "quiz-2") {
        index = 2;
    } else if (subject == "quiz-3") {
        index = 3;
    }
    for(let i=1; i<=data.length - 1; i++){
        obj = {
            email : data[i][0],
            subject: subject,
            points: data[i][index]
        }
        console.log(obj)
    }
}

viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

// SOAL 3
function recapScores(data) {
    let sequence='';
    let rata = 0;
    let predikat = '';
    for(let i=1; i<=data.length-1; i++){
        rata = (data[i][1] + data[i][2] + data[i][3]) / 3
        if (rata > 90) {
            predikat = `honour`;
        } else if (rata > 80) {
            predikat = `graduate`;
        } else if (rata > 70) {
            predikat = `participant`;
        }
        console.log(`${i}. Email : ${data[i][0]}`);
        console.log(`Rata - rata : ${rata}`);
        console.log(`Predikat : ${predikat}`);
        console.log('');
    }
    return sequence;
}

recapScores(data);



// LINK EDITOR : https://repl.it/@dazelpro/Quiz-2




