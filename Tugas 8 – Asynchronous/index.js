// di index.js
var readBooks = require('./callback.js');

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];

var time = 10000;
function countDown(x) {
    if(x == books.length){
        return 0;
    }
    readBooks(time, books[x], function(callback){
        time -= books[x].timeSpent;
        countDown(x+1)
    })
}
countDown(0)


// LINK EDITOR : https://repl.it/@dazelpro/Callback


