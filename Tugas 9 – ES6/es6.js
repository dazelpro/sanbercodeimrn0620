// SOAL 1 - FUNGSI ARROW
const fungsiArrowES6 = () => { 
    console.log("this is golden!!"); 
};

fungsiArrowES6();

// SOAL 2 - OBJECT LITERAL
const firstNameOL = 'William';
const lastNameOL = 'Imoh';
const fullName = {
    firstNameOL, lastNameOL
}
console.log(firstNameOL + " " + lastNameOL)

// SOAL 3 - DESTRUCTURING
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation);

// SOAL 4 - ARRAY SPREADING
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let result = [...west, ...east];
console.log(result);

// SOAL 5 - TEMPLATE LITERALS
const planet = "earth"
const view = "glass"
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
console.log(before) 


// LINK EDITOR : https://repl.it/@dazelpro/ES6




