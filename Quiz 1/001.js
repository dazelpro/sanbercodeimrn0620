// Soal A
function balikString(data) {
    let kata='';
    for(let i=data.length-1; i>=0; i--){
        kata=kata+data[i];
    }
    return kata;
}
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah 

// Soal B
function palindrome(data) {
    let kata='';
    for(let i=data.length-1; i>=0; i--){
        kata=kata+data[i];
    }
    if(data == kata) {
        return true;
    } else {
        return false;
    }
}
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

// Soal C
function bandingkan(num1, num2) {
    if(!num1) {
        return -1;
    }
    if(!num2) {
        return 1;
    }
    if(num1 < 0 || num2 < 0) {
        return -1;
    }
    if(num1 == num2) {
        return -1;
    }
    if(num1 > num2) {
        return num1;
    }
    if(num1 < num2) {
        return num2;
    }
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

// Link Editor : https://repl.it/@dazelpro/Quiz-1-001


