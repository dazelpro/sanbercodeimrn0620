// Soal A
function AscendingTen(num) {
    if(!num) {
        return -1;
    }
    let range = "";
    let numOut = num + 9;
    for(num; num <= numOut; num++) {
        range+=num+' ';
    }
    return range;
}
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

  // Soal B
function DescendingTen(num) {
    if(!num) {
        return -1;
    }
    let range = "";
    let numOut = num - 9;
    for(num; num>=numOut; num--) {
        range+=num+' ';
    }
    return range;
}
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

// Soal C
function ConditionalAscDesc(reference, check) {
    let range = "";
    if(!reference || !check) {
        return -1;
    }
    if (check%2!=0) {
        let numOut = reference + 9;
        for(reference; reference <= numOut; reference++) {
            range+=reference+' ';
        }
        return range;
    } else {
        let numOut = reference - 9;
        for(reference; reference>=numOut; reference--) {
            range+=reference+' ';
        }
        return range;
    }
}
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

// Soal D
function ularTangga() {
    let ular = '';
    for (let i=100; i>=1; i--){
        ular += i+' ';
        if (i%10 == 0) {
            ular += '\n';
        };
    }
    return ular;
}
console.log(ularTangga())

// Link Editor : https://repl.it/@dazelpro/Quiz-1-002

